﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDataGridSync
{
    class SampleData
    {
        public string Data1 { get; set; }
        public string Data2 { get; set; }
        public string Data3 { get; set; }
        public string Data4 { get; set; }
        public string Data5 { get; set; }
    }
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        //! スクロール同期処理
        private DataGridScrollSynchronizer ScrollSynchronizer;

        public MainWindow()
        {
            InitializeComponent();

            var list = new List<SampleData>();
            for (int i = 0; i < 10; ++i)
            {
                var sampleData = new SampleData()
                {
                    Data1 = $"Data{i}-0",
                    Data2 = $"Data{i}-1",
                    Data3 = $"Data{i}-2",
                    Data4 = $"Data{i}-3",
                    Data5 = $"Data{i}-4",
                };
                list.Add(sampleData);
            }

            dataGrid1.ItemsSource = list;
            dataGrid2.ItemsSource = list;
            dataGrid3.ItemsSource = list;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            // データグリッドのスクロール同期を設定する。
            var dataGridList = new List<DataGrid>();
            dataGridList.Add(dataGrid1);
            dataGridList.Add(dataGrid2);
            dataGridList.Add(dataGrid3);
            ScrollSynchronizer = new DataGridScrollSynchronizer(dataGridList, SynchronizeDirection.Both);
        }
    }
}
